#!/bin/bash

#
# Author  : Bart 全部技術
# Version : 1.0.0
# License : MIT License
#


#
# Variables (make sure these are correct)
#

# Gerrit vars
export GERRIT_URL='***'                   # The FQDN for your Gerrit instance, e.g. gerrit.example.com

# GitLab vars
export GITLAB_TOKEN='***'                 # A GitLab API token
export GITLAB_FQDN='***'                  # The FQDN for your GitLab instance, e.g. gitlab.example.com
export GITLAB_URL="https://$GITLAB_FQDN"  # The GitLab URL, because I'm lazy (assuming HTTPS).
export GITLAB_NS='***'                    # This is the GitLab group ID, within the API referred to as namespace.
export GITLAB_VISIBILITY='private'        # Options: private, internal, or public. Private is the safest choice (will always work).

# Working directory
export WORK_DIR=$(pwd)                    # Set a variable for the working dir (the directory you're in at that moment)

# Get all Gerrit projects
export GERRIT_PROJECTS=$(ssh -p 29418 $GERRIT_URL gerrit ls-projects --type code)


# Run this loop for each Gerrit project
for i in $GERRIT_PROJECTS
do
  # Go to the working dir
  cd $WORK_DIR

  # Create a new directory, with -p (some projects might be in seperate folders).
  mkdir -p $i.git

  # Grab all data for that project
  git clone --mirror "ssh://$GERRIT_URL:29418/$i.git" "$i.git"

  # Set the GitLab path for the Gerrit project, replacing a / with a _ (in case of a folder).
  GPATH=$(echo $i | tr '/' '_')

  # Create a new project using the GitLab API, this gives me more control over the path.
  curl \
    --silent \
    --request POST \
    --header "PRIVATE-TOKEN: $GITLAB_TOKEN" \
    --url $GITLAB_URL/api/v4/projects \
    --data-urlencode "path=$GPATH" \
    --data-urlencode "visibility=${GITLAB_VISIBILITY}" \
    --data-urlencode "namespace_id=$GITLAB_NS" \
    --output $GPATH.json

  # Go to the Gerrit repository
  cd $WORK_DIR/$i.git
  
  # Add the new remote path
  git remote add gitlab git@$GITLAB_FQDN:bp/pm/ba/$GPATH.git

  # Upload the Gerrit project to the new GitLab project
  git push gitlab --mirror
done
