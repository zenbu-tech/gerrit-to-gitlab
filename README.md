# Gerrit to GitLab

A script for migrating your Gerrit Code Review projects to GitLab.

- [Blog article: Migrate Gerrit Code Review to GitLab (Zenbu Tech)](https://zenbu.tech/migrate-gerrit-code-review-to-gitlab/)

## Overview

This script will do the following:

- Download all projects from Gerrit to your local machine.
- Create new projects under a specific GitLab group (using the GitLab API).
- Upload all Gerrit projects to the corresponding GitLab project.

## Requirements

- Gerrit account with appropriate permissions, preferably admin permissions. This account needs to be configured with a public SSH key.
- GitLab account with write permissions on your target group.
- Both accounts need to be configured with a public SSH key.
- Specifically for the GitLab account, create an API token: https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html
- Sufficient storage, you will be downloading a lot of information from Gerrit so make sure there's enough space on the machine running the script (and obviously also on the machine running GitLab, but I'm going to assume you've provisioned the GitLab instance correctly).

## Usage

Before running the script, make sure these variables are set:

```bash
# Gerrit vars
export GERRIT_URL='***'                   # The FQDN for your Gerrit instance, e.g. gerrit.example.com

# GitLab vars
export GITLAB_TOKEN='***'                 # A GitLab API token
export GITLAB_FQDN='***'                  # The FQDN for your GitLab instance, e.g. gitlab.example.com
export GITLAB_URL="https://$GITLAB_FQDN"  # The GitLab URL, because I'm lazy (assuming HTTPS).
export GITLAB_NS='***'                    # This is the GitLab group ID, within the API referred to as namespace.
```

Once that's done, you can run the script:

```bash
$ ./gerrit2gitlab.sh
```

## License

MIT License, see `LICENSE` file for more details.
